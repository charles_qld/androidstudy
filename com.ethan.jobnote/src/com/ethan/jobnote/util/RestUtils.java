package com.ethan.jobnote.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.zip.GZIPInputStream;

public class RestUtils {

    private static Object getJson(String url, Map<String, String> parameters, boolean returnArray) {
        if (StringUtils.isEmpty(url)) {
            throw new IllegalArgumentException("URL is required.");
        }

        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpGet getRequest = new HttpGet(url);
        getRequest.setHeader("Accept", "application/json");
        // Use GZIP encoding
        getRequest.setHeader("Accept-Encoding", "gzip"); //
        Object obj = null;
        try {
            HttpResponse response = (HttpResponse) httpclient
                    .execute(getRequest);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream instream = entity.getContent();
                Header contentEncoding = response
                        .getFirstHeader("Content-Encoding");
                if (contentEncoding != null
                        && contentEncoding.getValue().equalsIgnoreCase("gzip")) {
                    instream = new GZIPInputStream(instream);
                }
                // convert content stream to a String
                String result = readStream(instream);
                instream.close();
                if (returnArray) {
                    obj = new JSONArray(result);
                } else {
                    obj = new JSONObject(result);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }


    public static JSONArray getJsonArray(String url, Map<String, String> parameters) {
        return (JSONArray) getJson(url, parameters, true);
    }

    public static JSONObject getJsonObject(String url, Map<String, String> parameters) {
        return (JSONObject) getJson(url, parameters, false);
    }

    private static String readStream(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}
