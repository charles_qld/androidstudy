package com.ethan.jobnote.service;

import android.util.Log;
import com.ethan.jobnote.db.Job;
import com.ethan.jobnote.util.RestUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cstone on 15/12/2013.
 */
public class RemoteJobService {

    private String protocol = "http";
    private String ip = "192.168.1.104";
    private String port = "8080";
    private String urlLoadAll = "/job/listAll/username/charles";

    private static RemoteJobService instance = null;

    public List<Job> loadAllJobs(String username) {
        JSONArray jsonJobs = RestUtils.getJsonArray(this.baseUrl() + urlLoadAll, null);
        List<Job> jobs = new ArrayList<Job>();
        if (jsonJobs == null || jsonJobs.length() == 0) {
            return null;
        } else {
            for (int i = 0; i < jsonJobs.length(); i++) {
                try {
                    Job job = this.covertFromJson((JSONObject) jsonJobs.get(i));
                    jobs.add(job);
                } catch (JSONException e) {
                    Log.i(RemoteJobService.class.getName(), "Error occurs when covert json object " + jsonJobs);
                    continue;
                }
            }
        }
        return jobs;
    }

    private Job covertFromJson(JSONObject json) {
        if (json == null) {
            return null;
        }
        Job job = new Job();
        try {
            job.setId(json.getLong("id"));
            job.setAdvertiser(json.getString("advertiser") == null ? null : json.getString("advertiser"));
            job.setTitle(json.getString("title") == null ? null : json.getString("title"));
            job.setCompany(json.getString("company") == null ? null : json.getString("company"));
            job.setUuid(json.getString("uuid") == null ? null : json.getString("uuid"));
            if (json.getString("updateTimestamp") != null) {
                job.setLastUpdated(new Timestamp(Long.valueOf(json.getString("updateTimestamp"))));
            }
            return job;
        } catch (JSONException e) {
            Log.e(this.getClass().getName(), e.getMessage());
            return null;
        }
    }

    public static RemoteJobService getInstance() {
        if (instance == null) {
            instance = new RemoteJobService();
        }
        return instance;
    }

    private String baseUrl() {
        return protocol + "://" + ip + ":" + port + "/";
    }

}
