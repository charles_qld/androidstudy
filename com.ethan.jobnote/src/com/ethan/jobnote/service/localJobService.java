package com.ethan.jobnote.service;

import android.content.Context;
import com.ethan.jobnote.db.Job;
import com.ethan.jobnote.db.JobDataSource;

import java.util.List;

/**
 * Created by cstone on 8/01/2014.
 */
public class LocalJobService {

    Context context = null;
    JobDataSource jobDataSource = null;

    public LocalJobService(Context context) {
        this.context = context;
        jobDataSource = new JobDataSource(context);
        jobDataSource.open();
    }

    public int updateLocalJobs(List<Job> remoteJobs) {
        if (remoteJobs == null || remoteJobs.isEmpty()) {
            return 0;
        }

        int count = 0;
        for (Job remoteJob : remoteJobs) {
            if (remoteJob.getUuid() == null || remoteJob.getLastUpdated() == null) {
                continue;
            }
            Job localJob = jobDataSource.getJobByUuid(remoteJob.getUuid());
            if (localJob == null) {
                Job newJob = jobDataSource.createJob(new Job());
                remoteJob.setId(newJob.getId());
                jobDataSource.updateJob(remoteJob);
                count++;
            } else {
                if (localJob.getLastUpdated().before(remoteJob.getLastUpdated())) {
                    remoteJob.setId(localJob.getId());
                    jobDataSource.updateJob(remoteJob);
                }
            }
        }
        return count;
    }
}