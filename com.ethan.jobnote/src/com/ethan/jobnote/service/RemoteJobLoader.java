package com.ethan.jobnote.service;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import com.ethan.jobnote.db.Job;

import java.util.List;

/**
 * Created by cstone on 16/12/2013.
 */
public class RemoteJobLoader extends AsyncTaskLoader<List<Job>> {

    private String username;

    public RemoteJobLoader(Context context, String username) {
        super(context);
        this.username = username;
    }

    @Override
    public List<Job> loadInBackground() {
        RemoteJobService jobServerService = RemoteJobService.getInstance();
        List<Job>  jobs = jobServerService.loadAllJobs(username);
        return jobs;
    }
}
