package com.ethan.jobnote;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.view.*;
import android.widget.EditText;
import com.ethan.jobnote.db.Job;
import com.ethan.jobnote.db.JobDataSource;

/**
 * A fragment representing a single Item detail screen. This fragment is either
 * contained in a {@link ItemListActivity} in two-pane mode (on tablets) or a
 * {@link ItemDetailActivity} on handsets.
 */
public class ItemDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String JOB_ID = "job_id";

    /**
     * The current job this fragment is presenting.
     */
    private Job job;
    private JobDataSource jobDataSource = null;

    private Callbacks mCallbacks;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_item_detail,
                container, false);

        this.jobDataSource = new JobDataSource(this.getActivity());
        jobDataSource.open();


        if (getArguments().containsKey(JOB_ID)) {
            String jobId = getArguments().getString(JOB_ID);
            if (jobId != null) {
                job = jobDataSource.getJobById(Long.valueOf(jobId));
            }
        }

        if (job != null) {
            EditText textAdvertiser = (EditText) rootView.findViewById(
                    R.id.txtAdvertiser);
            textAdvertiser.setText(job.getAdvertiser());
            EditText textTitle = (EditText) rootView.findViewById(
                    R.id.txtTitle);
            textTitle.setText(job.getTitle());
        }
        return rootView;
    }

    public interface Callbacks {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException(
                    "Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. Use NavUtils to allow users
                // to navigate up one level in the application structure. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back
                //
                NavUtils.navigateUpTo(this.getActivity(), new Intent(this.getActivity(), ItemListActivity.class));
                return true;
            case R.id.action_remove:
                jobDataSource.deleteJob(job);
                NavUtils.navigateUpTo(this.getActivity(), new Intent(this.getActivity(), ItemListActivity.class));
                return true;
            case R.id.action_save:
                save();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void save() {
        if (!validateJob()) {
            return;
        }

        if (job.getId() <= 0) {
            job = jobDataSource.createJob(job);
        } else {
            job = jobDataSource.updateJob(job);
        }

    }


    private boolean validateJob() {
        EditText txtTitle = (EditText) this.getView().findViewById(R.id.txtTitle);
        String title = txtTitle.getText().toString();
        EditText txtAdvertiser = (EditText) this.getView().findViewById(R.id.txtAdvertiser);
        String advertiser = txtAdvertiser.getText().toString();

        if (job == null )  {
            job = new Job();
        }

        if (title == null || title.trim().equals("")) {
            return false;
        } else {
            job.setTitle(title);
        }

        if (advertiser == null || advertiser.trim().equals("")) {
            return false;
        } else {
            job.setAdvertiser(advertiser);
        }

        return true;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.detail_activity_actions, menu);
    }
}
