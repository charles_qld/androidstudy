package com.ethan.jobnote.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA.
 * User: cstone
 * Date: 1/12/2013
 * Time: 11:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class JobOpenHelper extends SQLiteOpenHelper {

    public static final String TABLE_JOB = "job";
    public static final String COL_ID = "_id";
    public static final String COL_COMPANY = "company";
    public static final String COL_TITLE = "title";
    public static final String COL_POSTDATE = "postdate";
    public static final String COL_ADVERTISER = "advertiser";
    public static final String COL_UUID = "uuid";
    public static final String COL_LASTUPDATED = "lastUpdated";

    private static final String DATABASE_NAME = "job.db";
    private static final int DATABASE_VERSION = 4;

    private static final String DATABASE_CREATE = "create table "
            + TABLE_JOB + "("
            + COL_ID + " integer primary key autoincrement, "
            + COL_ADVERTISER + " advertiser not null, "
            + COL_TITLE + " text not null, "
            + COL_COMPANY + " text, "
            + COL_POSTDATE + " integer, "
            + COL_LASTUPDATED + " integer, "
            + COL_UUID + " text" +
            ");";

    public JobOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(JobOpenHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_JOB);
        onCreate(db);
    }
}
