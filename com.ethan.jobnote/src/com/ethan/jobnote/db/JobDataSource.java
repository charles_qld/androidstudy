package com.ethan.jobnote.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: cstone
 * Date: 1/12/2013
 * Time: 11:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class JobDataSource {

    private SQLiteDatabase db;
    private JobOpenHelper dbHelper;

    private String[] allColumns = {JobOpenHelper.COL_ID,
            JobOpenHelper.COL_TITLE,
            JobOpenHelper.COL_ADVERTISER,
            JobOpenHelper.COL_COMPANY,
            JobOpenHelper.COL_POSTDATE,
            JobOpenHelper.COL_UUID,
            JobOpenHelper.COL_LASTUPDATED};

    public JobDataSource(Context context) {
        dbHelper = new JobOpenHelper(context);
    }

    public void open() {
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Job createJob(Job job) {
        ContentValues values = new ContentValues();
        values.put(JobOpenHelper.COL_ADVERTISER, job.getAdvertiser());
        values.put(JobOpenHelper.COL_TITLE, job.getTitle());
        long longPostDate = job.getPostDate() == null ? System.currentTimeMillis() : job.getPostDate().getTime();
        values.put(JobOpenHelper.COL_POSTDATE, longPostDate);
        values.put(JobOpenHelper.COL_UUID, UUID.randomUUID().toString());
        values.put(JobOpenHelper.COL_LASTUPDATED, new Date().getTime());
        long insertId = db.insert(JobOpenHelper.TABLE_JOB, null,
                values);
        if (insertId < 0) {
            return null;
        }

        Cursor cursor = db.query(JobOpenHelper.TABLE_JOB,
                allColumns, JobOpenHelper.COL_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Job newJob = cursorToJob(cursor);
        cursor.close();
        return newJob;
    }

    public Job updateJob(Job job) {
        if (job.getId() <= 0) {
            return null;
        }
        ContentValues values = new ContentValues();
        values.put(JobOpenHelper.COL_ADVERTISER, job.getAdvertiser());
        values.put(JobOpenHelper.COL_TITLE, job.getTitle());
        values.put(JobOpenHelper.COL_LASTUPDATED, new Date().getTime());
        long updatedId = db.update(JobOpenHelper.TABLE_JOB, values, JobOpenHelper.COL_ID + " = ?", new String[]{String.valueOf(job.getId())});
        Cursor cursor = db.query(JobOpenHelper.TABLE_JOB,
                allColumns, JobOpenHelper.COL_ID + " = " + job.getId(), null,
                null, null, null);
        cursor.moveToFirst();
        Job updatedJob = cursorToJob(cursor);
        cursor.close();
        return updatedJob;
    }

    private Job cursorToJob(Cursor cursor) {
        Job job = new Job();
        job.setId(cursor.getLong(0));
        job.setTitle(cursor.getString(1));
        job.setAdvertiser(cursor.getString(2));
        job.setCompany(cursor.getString(3));
        job.setPostDate(new Date(cursor.getLong(4)));
        return job;
    }

    public void deleteJob(Job job) {
        long id = job.getId();
        System.out.println("Job deleted with id: " + id);
        db.delete(JobOpenHelper.TABLE_JOB, JobOpenHelper.COL_ID
                + " = " + id, null);
    }

    public void clearJobs() {
        System.out.println("Jobs cleared");
        db.delete(JobOpenHelper.TABLE_JOB, null, null);
    }

    public List<Job> getAllJobs() {
        List<Job> jobs = new ArrayList<Job>();

        Cursor cursor = db.query(JobOpenHelper.TABLE_JOB,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Job job = cursorToJob(cursor);
            jobs.add(job);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return jobs;
    }

    public Job getJobById(long jobId) {
        List<Job> jobs = new ArrayList<Job>();

        Cursor cursor = db.query(JobOpenHelper.TABLE_JOB,
                allColumns, JobOpenHelper.COL_ID + " = ?", new String[]{String.valueOf(jobId)}, null, null, null);
        cursor.moveToFirst();
        Job job = null;
        if (!cursor.isAfterLast()) {
            job = cursorToJob(cursor);
        }
        cursor.close();
        return job;
    }

    public Job getJobByUuid(String uuid) {
        List<Job> jobs = new ArrayList<Job>();

        Cursor cursor = db.query(JobOpenHelper.TABLE_JOB,
                allColumns, JobOpenHelper.COL_UUID + " = ?", new String[]{uuid}, null, null, null);
        cursor.moveToFirst();
        Job job = null;
        if (!cursor.isAfterLast()) {
            job = cursorToJob(cursor);
        }
        cursor.close();
        return job;
    }

}
