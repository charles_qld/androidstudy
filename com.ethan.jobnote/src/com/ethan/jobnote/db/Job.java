package com.ethan.jobnote.db;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class Job implements Serializable {

    private static final long serialVersionUID = -8168963008419423879L;

    private long id;
    private String company;
    private String title;
    private Date postDate;
    private Date applyDate;
    private String location;
    private String advertiser;
    private Timestamp lastUpdated;
    private String uuid;

    public Job() {
    }

    public Job(String advertiser, String title) {
        super();
        this.advertiser = advertiser;
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public Date getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(Date applyDate) {
        this.applyDate = applyDate;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAdvertiser() {
        return advertiser;
    }

    public void setAdvertiser(String advertiser) {
        this.advertiser = advertiser;
    }

    @Override
    public String toString() {
        return title + " [" + advertiser + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Job.class != o.getClass()) return false;

        Job job = (Job) o;

        if (id != job.id) return false;
        if (advertiser != null ? !advertiser.equals(job.advertiser) : job.advertiser != null) return false;
        if (applyDate != null ? !applyDate.equals(job.applyDate) : job.applyDate != null) return false;
        if (company != null ? !company.equals(job.company) : job.company != null) return false;
        if (lastUpdated != null ? !lastUpdated.equals(job.lastUpdated) : job.lastUpdated != null) return false;
        if (location != null ? !location.equals(job.location) : job.location != null) return false;
        if (postDate != null ? !postDate.equals(job.postDate) : job.postDate != null) return false;
        if (title != null ? !title.equals(job.title) : job.title != null) return false;
        if (uuid != null ? !uuid.equals(job.uuid) : job.uuid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (company != null ? company.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (postDate != null ? postDate.hashCode() : 0);
        result = 31 * result + (applyDate != null ? applyDate.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (advertiser != null ? advertiser.hashCode() : 0);
        result = 31 * result + (lastUpdated != null ? lastUpdated.hashCode() : 0);
        result = 31 * result + (uuid != null ? uuid.hashCode() : 0);
        return result;
    }

    public Timestamp getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Timestamp lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
