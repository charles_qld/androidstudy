package com.ethan.jobnote;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.ethan.jobnote.db.Job;
import com.ethan.jobnote.db.JobDataSource;
import com.ethan.jobnote.service.LocalJobService;
import com.ethan.jobnote.service.RemoteJobLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * A list fragment representing a list of Items. This fragment also supports
 * tablet devices by allowing list items to be given an 'activated' state upon
 * selection. This helps indicate which item is currently being viewed in a
 * {@link ItemDetailFragment}.
 * <p/>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class ItemListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<List<Job>> {

    /**
     * The serialization (saved instance state) Bundle key representing the
     * activated item position. Only used on tablets.
     */
    private static final String STATE_ACTIVATED_POSITION = "activated_position";

    /**
     * The fragment's current callback object, which is notified of list item
     * clicks.
     */
    private Callbacks mCallbacks;

    /**
     * The current activated item position. Only used on tablets.
     */
    private int mActivatedPosition = ListView.INVALID_POSITION;

    private List<Job> jobs = new ArrayList<Job>();

    private JobDataSource jobDataSource;

    private LocalJobService localJobService;

    private String username = "";

    @Override
    public android.support.v4.content.Loader<List<Job>> onCreateLoader(int i, Bundle bundle) {
        return new RemoteJobLoader(this.getActivity().getApplicationContext(), username);
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<List<Job>> listLoader, List<Job> jobs) {
        Log.i(ListFragment.class.getName(), "onLoadFisihed");
        if (jobs != null && !jobs.isEmpty()) {
            int updatedCount = localJobService.updateLocalJobs(jobs);
            Log.i(ItemListFragment.class.getName(), "Saved " + updatedCount + " jobs into local database.");
        }
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<List<Job>> listLoader) {
        Log.i(ListFragment.class.getName(), "onLoaderReset");

    }


    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        public void onItemSelected(String id);
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.jobDataSource == null) {
            jobDataSource = new JobDataSource(this.getActivity());
            jobDataSource.open();
        }
        refreshList();

        if (localJobService == null) {
            localJobService = new LocalJobService(this.getActivity());
        }
    }

    private void refreshList() {
        this.jobs = loadJobs();
        setListAdapter(new ArrayAdapter<Job>(getActivity(),
                android.R.layout.simple_list_item_activated_1,
                android.R.id.text1, jobs));
    }

    public List<Job> loadJobs() {
        //TODO: temporary code for debugging
        if (jobDataSource.getAllJobs().size() == 0) {
            jobDataSource.clearJobs();
            jobDataSource.createJob(new Job("PIPE Networks", "Analysis Programmer"));
            jobDataSource.createJob((new Job("IBM", "Consultant")));
            jobDataSource.createJob((new Job("TPG", "Business Analyst")));
        }

        return jobDataSource.getAllJobs();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Restore the previously serialized activated item position.
        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState
                    .getInt(STATE_ACTIVATED_POSITION));
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException(
                    "Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = null;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position,
                                long id) {
        super.onListItemClick(listView, view, position, id);

        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
        Job clickedJob = (Job) listView.getItemAtPosition(position);
        mCallbacks.onItemSelected(String.valueOf(clickedJob.getId()));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    /**
     * Turns on activate-on-click mode. When this mode is on, list items will be
     * given the 'activated' state when touched.
     */
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        getListView().setChoiceMode(
                activateOnItemClick ? ListView.CHOICE_MODE_SINGLE
                        : ListView.CHOICE_MODE_NONE);
    }

    private void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            getListView().setItemChecked(mActivatedPosition, false);
        } else {
            getListView().setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }

    @Override
    public void onResume() {
        this.refreshList();
        super.onResume();

    }

    /* (non-Javadoc)
     * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                Intent detailIntent = new Intent(this.getActivity(), ItemDetailActivity.class);
                startActivity(detailIntent);
                break;
            case R.id.action_remove:
                Job selectedJob = (Job) getListView().getSelectedItem();
                jobDataSource.deleteJob(selectedJob);
                this.refreshList();
                break;
            case R.id.action_sync:
                this.startRemoteLoader();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    public void startRemoteLoader() {
        Loader<List<Job>> loader = getLoaderManager().initLoader(0, new Bundle(), this);
        loader.forceLoad();
    }
}
