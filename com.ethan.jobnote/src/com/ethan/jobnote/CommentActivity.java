package com.ethan.jobnote;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;

/**
 * Created by cstone on 8/12/2013.
 */
public class CommentActivity extends Activity {

    public static final String TIP = "tip";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        Object tips = getIntent().getExtras().get(CommentActivity.TIP);
        String inputTip = tips == null ? "" : (String) getIntent().getExtras().get(CommentActivity.TIP);
        EditText editText = (EditText) findViewById(R.id.textComment);
        editText.setText(inputTip);
    }
}