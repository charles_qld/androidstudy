package com.ethan.jobnote;

import android.test.ActivityInstrumentationTestCase2;

/**
 * This is a simple framework for a test of an Job.  See
 * {@link android.test.ApplicationTestCase ApplicationTestCase} for more information on
 * how to write and extend Job tests.
 * <p/>
 * To run this test, you can type:
 * adb shell am instrument -w \
 * -e class com.ethan.jobnote.ItemListActivityTest \
 * com.ethan.jobnote.tests/android.test.InstrumentationTestRunner
 */
public class ItemListActivityTest extends ActivityInstrumentationTestCase2<ItemListActivity> {

    private ItemListActivity activity;

    public ItemListActivityTest() {
        super("com.ethan.jobnote", ItemListActivity.class);
    }


    public void testOnItemSelected  () {

    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        setActivityInitialTouchMode(false);
        activity = getActivity();
    }
}
