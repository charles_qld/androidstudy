package com.ethan.jobserver.service;

import com.ethan.jobserver.domain.Job;
import com.ethan.jobserver.domain.User;
import com.ethan.jobserver.repository.JobRepository;
import com.ethan.jobserver.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: cstone
 * Date: 11/12/2013
 * Time: 11:43 PM
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class JobServiceImplTest {

    @Autowired
    JobRepository jobRepositoryMock;

    @Autowired
    UserRepository userRepositoryMock;

    @Autowired
    @Qualifier("JobServiceImpl")
    JobService jobService;

    @Before
    public void setup() {
        ReflectionTestUtils.setField(jobService, "userRepository", userRepositoryMock);
        ReflectionTestUtils.setField(jobService, "jobRepository", jobRepositoryMock);
    }

    @Test
    public void findByIdTest() {
        User user = prepareUser();
        Job job = prepareJob(user);

        reset(userRepositoryMock);
        reset(jobRepositoryMock);
        expect(userRepositoryMock.findByUsername(user.getUsername())).andReturn(user);
        expect(jobRepositoryMock.findByIdAndUser(job.getId(), user)).andReturn(job);
        replay(userRepositoryMock);
        replay(jobRepositoryMock);

        Job resultJob = jobService.findById(job.getId(), user.getUsername());
        assertNotNull(resultJob);
        assertEquals(job.getId(), resultJob.getId());
    }


    @Test
    public void listAllTest() {
        User user = prepareUser();
        List<Job> jobs = new ArrayList<Job>(3);
        jobs.add(prepareJob(user));
        jobs.add(prepareJob(user));
        jobs.add(prepareJob(user));

        reset(userRepositoryMock);
        reset(jobRepositoryMock);
        expect(userRepositoryMock.findByUsername(user.getUsername())).andReturn(user);
        expect(jobRepositoryMock.findByUser(user)).andReturn(jobs);
        replay(userRepositoryMock);
        replay(jobRepositoryMock);

        List<Job> resultJobs = jobService.listAll(user.getUsername());
        assertNotNull(resultJobs);
        assertEquals(resultJobs.size(), 3);
    }

    @Test
    public void removeJobTest() {
        User user = prepareUser();
        Job job = prepareJob(user);

        reset(userRepositoryMock);
        reset(jobRepositoryMock);
        expect(userRepositoryMock.findByUsername(user.getUsername())).andReturn(user);
        expect(jobRepositoryMock.findByIdAndUser(job.getId(), user)).andReturn(job);
        replay(userRepositoryMock);
        replay(jobRepositoryMock);

        jobService.removeJob(job.getId(), user.getUsername());
    }

    @Test
    public void addJobTest() {
        User user = prepareUser();
        Job job = prepareJob(user);
        job.setId(0);
        Job job1 = new Job();
        BeanUtils.copyProperties(job, job1);
        job1.setId(1l);

        reset(userRepositoryMock);
        reset(jobRepositoryMock);
        expect(userRepositoryMock.findByUsername(user.getUsername())).andReturn(user);
        expect(jobRepositoryMock.save(job)).andReturn(job1);
        replay(userRepositoryMock);
        replay(jobRepositoryMock);

        Job result = jobService.addJob(job, user.getUsername());
        assertNotNull(result);
        assertTrue(result.getId() > 0);
    }

    @Test
    public void updateJobTest() {
        User user = prepareUser();
        Job job = prepareJob(user);
        Job job1 = new Job();
        BeanUtils.copyProperties(job, job1);
        job1.setTitle("New title");

        reset(userRepositoryMock);
        reset(jobRepositoryMock);
        expect(userRepositoryMock.findByUsername(user.getUsername())).andReturn(user);
        expect(jobRepositoryMock.findByIdAndUser(job.getId(), user)).andReturn(job1);
        expect(jobRepositoryMock.save(job)).andReturn(job1);
        replay(userRepositoryMock);
        replay(jobRepositoryMock);

        Job result = jobService.addJob(job, user.getUsername());
        assertNotNull(result);
        assertTrue(result.getId() > 0);
        assertEquals(result.getTitle(), job1.getTitle());
    }


    private Job prepareJob(User user) {
        long id = System.currentTimeMillis();
        Job job = new Job();
        job.setId(id);
        job.setUser(user);
        job.setTitle("Software Engineer" + id);
        job.setAdvertiser("PIPE Networks" + id);
        return job;
    }

    private User prepareUser() {
        User user = new User();
        user.setId(1l);
        user.setUsername("charles");
        user.setFirstName("Charles");
        user.setLastName("Shi");
        user.setEmail("chareles.shi@gmail.com");
        user.setCity("Brisbane");
        user.setCountry("Australia");
        return user;
    }

    @Test
    public void generateUUID(){
        UUID uuid = UUID.randomUUID();
        System.out.println(uuid.toString());
    }
}
