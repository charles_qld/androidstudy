package com.ethan.jobserver.repository;

import com.ethan.jobserver.domain.Job;
import com.ethan.jobserver.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: cstone
 * Date: 11/12/2013
 * Time: 11:13 PM
 * To change this template use File | Settings | File Templates.
 */
public interface JobRepository extends JpaRepository<Job, Long> {
    public Job findByIdAndUser(long jobId, User user);

    public List<Job> findByUser(User user);
}
