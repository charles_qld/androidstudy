package com.ethan.jobserver.service;

import com.ethan.jobserver.domain.Job;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: cstone
 * Date: 11/12/2013
 * Time: 11:16 PM
 * To change this template use File | Settings | File Templates.
 */
public interface JobService {
    public Job findById(long jobId, String username);

    public List<Job> listAll(String username);

    public boolean removeJob(long jobId, String username);

    public Job addJob(Job job, String username);

    public Job updateJob(Job job, String username);
}
