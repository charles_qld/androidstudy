package com.ethan.jobserver.service;

import com.ethan.jobserver.domain.Job;
import com.ethan.jobserver.domain.User;
import com.ethan.jobserver.repository.JobRepository;
import com.ethan.jobserver.repository.UserRepository;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: cstone
 * Date: 11/12/2013
 * Time: 11:17 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
@Qualifier("JobServiceImpl")
public class JobServiceImpl implements JobService {
    @Autowired
    JobRepository jobRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Job findById(long jobId, String username) {
        if (StringUtils.isEmpty(username)) {
            return null;
        }
        User user = userRepository.findByUsername(username);
        if (user == null) {
            return null;
        }

        Job job = jobRepository.findByIdAndUser(jobId, user);
        return job;
    }

    @Override
    public List<Job> listAll(String username) {
        if (StringUtils.isEmpty(username)) {
            return null;
        }
        User user = userRepository.findByUsername(username);
        if (user == null) {
            return null;
        }

        List<Job> jobs = jobRepository.findByUser(user);
        return jobs;
    }

    @Override
    public boolean removeJob(long jobId, String username) {
        if (StringUtils.isEmpty(username)) {
            return false;
        }
        User user = userRepository.findByUsername(username);
        if (user == null) {
            return false;
        }

        Job job = jobRepository.findByIdAndUser(jobId, user);
        if (job == null) {
            return false;
        }

        jobRepository.delete(job);
        return true;
    }

    @Override
    public Job addJob(Job job, String username) {
        if (StringUtils.isEmpty(username) || job == null) {
            return null;
        }

        User user = userRepository.findByUsername(username);
        if (user == null) {
            return null;
        }

        job.setUser(user);
        Job newJob = jobRepository.save(job);
        return newJob;
    }

    @Override
    public Job updateJob(Job job, String username) {
        if (StringUtils.isEmpty(username) || job == null) {
            return null;
        }

        User user = userRepository.findByUsername(username);
        if (user == null) {
            return null;
        }

        Job jobInDb = jobRepository.findByIdAndUser(job.getId(), user);
        if (jobInDb == null) {
            return null;
        }

        jobInDb = jobRepository.save(job);
        return jobInDb;
    }
}
