package com.ethan.jobserver.controller;

import com.ethan.jobserver.domain.Job;
import com.ethan.jobserver.service.JobService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: cstone
 * Date: 12/12/2013
 * Time: 4:14 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/job")
public class JobController {

    @Autowired
    JobService jobService;

    @RequestMapping(value = "/saveJob/username/{username}", method = RequestMethod.POST)
    @ResponseBody
    public String saveJob(@PathVariable String username, @RequestBody String data, HttpServletResponse response) {
        Job job = new Gson().fromJson(data, Job.class);
        Job savedJob = null;
        if (job.getId() > 0) {
            savedJob = jobService.updateJob(job, username);
        } else {
            savedJob = jobService.addJob(job, username);
        }
        return new GsonBuilder().setPrettyPrinting().create().toJson(savedJob);

    }

    @RequestMapping("/listAll/username/{username}")
    @ResponseBody
    public String listJob(@PathVariable("username") String username) {
        List<Job> jobs = jobService.listAll(username);
        if (CollectionUtils.isNotEmpty(jobs)) {
            for (Job job : jobs) {
                if (job.getLastUpdated() != null) {
                    job.setUpdateTimestamp(job.getLastUpdated().getTime());
                }
            }
            return new GsonBuilder().setPrettyPrinting().create().toJson(jobs);
        } else {
            return new Gson().toJson(new ArrayList<Job>());
        }
    }

    @RequestMapping("/remove/username/{username}/jobId/{jobId}")
    @ResponseBody
    public String removeJob(@PathVariable("username") String username,
                            @PathVariable("jobId") long jobId) {
        boolean success = jobService.removeJob(jobId, username);
        return String.valueOf(success);
    }


    @RequestMapping("/getJob/username/{username}/jobId/{jobId}")
    @ResponseBody
    public String getJob(@PathVariable("username") String username,
                         @PathVariable("jobId") long jobId) {
        Job job = jobService.findById(jobId, username);
        if (job != null) {
            return new GsonBuilder().setPrettyPrinting().create().toJson(job);
        } else {
            return new Gson().toJson(new Object());
        }

    }

}
