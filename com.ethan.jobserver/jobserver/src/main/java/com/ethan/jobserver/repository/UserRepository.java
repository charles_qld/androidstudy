package com.ethan.jobserver.repository;

import com.ethan.jobserver.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created with IntelliJ IDEA.
 * User: cstone
 * Date: 13/12/2013
 * Time: 1:50 PM
 * To change this template use File | Settings | File Templates.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    public User findByUsername(String username);

    public User findByUsernameAndPassword(String username, String password);
}
